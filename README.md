# CI/CD Pipeline for SimpleApp

This repository contains a basic Continuous Integration and Continuous Deployment (CI/CD) pipeline using GitLab, Jenkins, and a Jenkinsfile for a standalone application.

## Requirements

### 1. Setup GitLab Repository

- Create a new GitLab repository named "SimpleApp."
- Initialize the repository with a simple README.md file.

### 2. Sample Application

- Create a simple standalone application in any programming language of your choice (e.g., Python, Java, Node.js, etc.).
- The application should perform a basic task, such as printing a message or performing a simple calculation.

### 3. Jenkins Setup

- Install Jenkins on your local machine or on a server of your choice.
- Configure Jenkins to integrate with your GitLab repository.

### 4. Create Jenkins Pipeline using Jenkinsfile

- Create a Jenkins pipeline using the provided Jenkinsfile.
- The pipeline includes stages for:
  - **Checkout:** Checkout the source code from the GitLab repository.
  - **Build:** Build the standalone application.
  - **Test:** Execute any basic tests for the application (e.g., syntax checks, linting).
  - **Deploy:** Deploy the application to a test environment (e.g., localhost).

### 5. GitLab CI Configuration

- Create a `.gitlab-ci.yml` file in the root of your GitLab repository.
- Configure GitLab CI to trigger the Jenkins pipeline on each commit to the repository.

## Getting Started

1. **Clone Repository:**
   ```bash
   git clone <repository_url>
   cd SimpleApp
   ```

2. **Run Application Locally:**
   - Follow the instructions in the application's README to run it locally.

3. **Make Changes:**
   - Modify the application code or Jenkinsfile as needed.

4. **Commit Changes:**
   ```bash
   git add .
   git commit -m "Add meaningful commit message"
   git push origin master
   ```

5. **Monitor CI/CD Pipeline:**
   - Visit your Jenkins instance to monitor the CI/CD pipeline triggered by the GitLab CI/CD configuration.

## Jenkinsfile

```groovy
pipeline {
    agent { 
        node {
            label 'my_pc'
        }
    }
    
    stages {
        stage('Checkout') {
            steps {
                checkout scm
            }
        }
        
        stage('Build') {
            steps {
                script {
                    sh '''#!/bin/bash
                    npm install
                    '''
                }
            }
        }
        
        stage('Test') {
            steps {
                script {
                    sh '''#!/bin/bash
                    npm run lint -- --fix
                    '''
                }
            }
        }
        
        stage('Deploy') {
            steps {
                script {
                    sh '''#!/bin/bash
                    node app.js
                    '''
                }
            }
        }
    }
}
```

This Jenkinsfile defines a CI/CD pipeline for the SimpleApp project. It includes stages for checking out the code, building the application, running tests, and deploying the application. The pipeline is configured to run on a node labeled 'my_pc'. The stages are executed sequentially, and each stage contains relevant steps defined using the script block.
