const express = require('express');
const fs = require('fs/promises');

const app = express();
const port = 3000;

app.use(express.urlencoded({extended: true}));
app.use(express.json());

const fileName = 'data.txt';

app.get('/', async (req, res) => {
  try {
    const data = await fs.readFile(fileName, 'utf-8');
    const names = data.split('\n').filter((name) => name.trim() !== '');
    res.send(`Stored Names: ${names.join(', ')}`);
  } catch (error) {
    console.error(error);
    res.status(500).send('Internal Server Error');
  }
});

app.post('/addName', async (req, res) => {
  const {name} = req.body;

  if (!name) {
    return res.status(400).send('Name is required.');
  }

  try {
    await fs.appendFile(fileName, `${name}\n`);
    res.send(`Name "${name}" added successfully.`);
  } catch (error) {
    console.error(error);
    res.status(500).send('Internal Server Error');
  }
});

app.listen(port, () => {
  console.log(`Server is running at http://localhost:${port}`);
});
